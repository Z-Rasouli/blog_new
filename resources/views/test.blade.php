<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled Document</title>
</head>

<body>
    <h1>test Heading</h1>
    <h6>laravel training</h6>
    <ul>
        @foreach($names as $name)
        <li>
            @if($name == "gholi")
            <b><i>{{$name}}</i></b>
            @else
            {{$name}}
            @endif
        </li>
        @endforeach

        {{$a??'test'}}
        @php($A= 20)
        <?php
        $b = 5454;
        ?>
        {{$A}}
    </ul>
</body>

</html>